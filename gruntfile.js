﻿var fs = require('fs');
module.exports = function(grunt) {
    'use strict';

    var version = (function() {
        var now = new Date,
            version = {
                year: now.getFullYear(),
                month: now.getMonth() + 1,
                date: now.getDate(),
                hour: now.getHours(),
                minute: now.getMinutes()
            };
        ['month', 'date', 'hour', 'minute'].forEach(function(key) {
            if (version[key] < 10) {
                version[key] = '0' + version[key];
            }
        });
        return [version.year, version.month, version.date, version.hour, version.minute].join('');
    })();
    fs.writeFileSync(__dirname + '/config/version.json', '{"version": "' + version + '"}');
    var lessSrc = ['public/css/main.less'],
        lessDestSrc = '',
        cssminSrc = ['public/css/main.css'],
        concatSrc = ['public/js/*.js', '!public/js/main.js'],
        concatDestSrc = 'public/js/main.js',
        uglifySrc = ['public/js/main.js'],
        replaceSrc = ['public/index.html'],
        watchLessSrc = ['public/css/*.less'],
        watchServerSrc = ['app.js', 'controller/*.js'];

    //任务配置,所有插件的配置信息
    grunt.file.preserveBOM = true;
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        //less插件配置
        less: {
            main: {
                expand: true,
                src: lessSrc,
                dest: lessDestSrc,
                ext: '.css'
            },
            dev: {
                options: {
                    compress: true,
                    yuicompress: false
                }
            }
        },

        //css压缩插件
        cssmin: {
            target: {
                files: {
                    'public/dist/main.min.css': cssminSrc
                }
            }
        },

        concat: {
            dist: {
                src: concatSrc,
                dest: concatDestSrc
            }
        },
        //uglify插件的配置信息(js压缩插件)
        uglify: {
            options: {
                stripBanners: true,
                banner: '/*|<%=pkg.name%>-<%=pkg.version%>.js <%=grunt.template.today("yyyy-mm-dd")%>*/\n'
            },
            my_target: {
                files: {
                    'public/dist/main.min.js': uglifySrc
                }
            }
        },

        //for html version
        replace: {
            dist: {
                options: {
                    patterns: [{
                        match: /version=([^\']+?)\"/g,
                        replacement: function() {
                            return 'version=' + version + '"'; // replaces "foo" to "bar"
                        }
                    }]
                },
                files: [{
                    expand: true,
                    src: replaceSrc
                }]
            }
        },

        //for dev launch
        develop: {
            server: {
                file: 'app.js' //the file to start server
            }
        },

        //watch插件的配置信息(监控js,css文件,如改变自动压缩,语法检查)
        watch: {
            public: { //用于监听less文件,当改变时自动编译成css文件
                files: watchLessSrc,
                tasks: ['less', 'cssmin'],
                options: {
                    livereload: true
                }
            },
            build: {
                files: concatSrc,
                tasks: ['concat', 'uglify'],
                options: {
                    livereload: true,
                    spawn: false
                }
            },
            js: {
                files: watchServerSrc, //can use regex to match file
                tasks: ['develop'],
                options: {
                    nospawn: true //is important !You need to specify nospawn : true in your watch task configuration so that the called tasks run in the same context.
                }
            }
        }
    });

    //告诉grunt我们将使用插件
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-replace');
    grunt.loadNpmTasks('grunt-develop');

    //告诉grunt当我们在终端中输入grunt时需要做些什么(注意先后顺序)
    grunt.registerTask('default', ['less', 'cssmin', 'concat', 'uglify', 'develop', 'watch']);
    grunt.registerTask('release', ['less', 'cssmin', 'concat', 'uglify', 'replace']);

};