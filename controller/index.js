var express = require('express')
    ,Router = express.Router
    ,bodyParser = require('body-parser');

module.exports = Router()
    .use(bodyParser.urlencoded({extended: true}))
    .use(bodyParser.json())//body data json
    .get('/index', function(req, res, next) {
        res.sendfile('./public/index.html');
    });