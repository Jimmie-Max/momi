var config = require('./config_' + (process.env.NODE_ENV || 'dev'));
config.version = require('./version.json').version;

module.exports = config;