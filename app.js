'use strict';
var express = require('express'),
    config = require('./config');
express()
    .use(express.static(__dirname + '/public', {
        dotfiles: 'ignore',
        etag: true,
        extensions: false,
        index: 'index.html',
        lastModified: true,
        maxAge: 1000 * 3600 * 24 * 30,
        redirect: true,
        setHeaders: function(res, path) {
            res.setHeader('Access-Control-Allow-Origin', '*');
        }
    }))
    .use('/', require('./controller'))
    .listen(config.port);
console.log('app listening on port %d', config.port);